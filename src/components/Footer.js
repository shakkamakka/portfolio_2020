import React from "react";
import styled from "styled-components";
import { MdPictureAsPdf } from "react-icons/md";
import { A, Love } from ".";

const Section = styled.div`
  color: ${(props) => (props.theme.mode === "light" ? `#333` : `pink`)};
  height: ${(props) => props.theme.footerheight};
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 10px;
  line-height: 30px;
`;
const Left = styled.div`
  width: 20%;
  text-align: left;
`;
const Center = styled.div`
  display: flex;
  justify-content: space-between;
`;
const Right = styled.div`
  width: 20%;
  display: flex;
  justify-content: flex-end;
  padding-right: 20px;
  & a {
    color: ${(props) => props.theme.color4};
    text-decoration: none;
    text-align: right;
  }
  &:hover {
    text-decoration: underline;
    color: ${(props) => props.theme.color3};
  }
  &:visited {
    color: ${(props) => props.theme.color4};
  }
`;

const Footer = () => {
  return (
    <Section>
      <Left></Left>
      <Center>
        <span>
          Made with <Love /> and
        </span>
        <span> </span>
        <A href="https://reactjs.org/" target="blank">
          React.js
        </A>
      </Center>
      <Right>
        <A target="_blank" href="./assets/CV_2025.pdf" download>
          <MdPictureAsPdf /> My resume
        </A>
      </Right>
    </Section>
  );
};

export default Footer;
