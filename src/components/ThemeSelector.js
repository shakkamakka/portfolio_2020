import React from "react";
import styled, { withTheme } from "styled-components";
import { IoMdPlanet } from "react-icons/io";
import { GiSun } from "react-icons/gi";
import { BsFillMoonFill } from "react-icons/bs";
import useSound from "use-sound";

const ThemeList = styled.div`
  display: flex;
  & * {
    transition: all 0.5s;
  }
  button {
    background: none;
    border: none;
    cursor: pointer;
    color: ${(props) => props.theme.color};
    opacity: 0.5;
  }
  button:hover {
    opacity: 1;
  }
  button:hover svg {
    filter: drop-shadow(
      0px 0px 5px
        ${(props) =>
          props.theme.mode === "light" ? "#fff" : props.theme.color}
    );
  }
  button.active:hover svg {
    filter: drop-shadow(
      0px 0px 5px
        ${(props) =>
          props.theme.mode === "light" ? "#fff" : props.theme.themeActive}
    );
  }
  button.active {
    color: ${(props) => props.theme.themeActive};
    opacity: 1;
  }
  button svg.smaller {
    font-size: 15px;
  }
  button svg {
    font-size: 20px;
  }
`;

const ThemeSelector = (props) => {
  const theme = props.theme.mode;
  const [play] = useSound("/assets/lightswitch.mp3", {
    volume: 0.05,
    sprite: {
      light: [0, 300],
      dark: [500, 300],
      space: [500, 1000],
    },
  });

  return (
    <ThemeList>
      <button
        onClick={() => {
          props.setLocalTheme("light");
          localStorage.setItem("theme", "light");
          play({ id: "light" });
        }}
        className={theme === "light" ? "active" : ""}
      >
        <GiSun />
      </button>
      <button
        onClick={() => {
          props.setLocalTheme("dark");
          localStorage.setItem("theme", "dark");
          play({ id: "dark" });
        }}
        className={theme === "dark" ? "active" : ""}
      >
        <BsFillMoonFill className="smaller" />
      </button>
      <button
        onClick={() => {
          props.setLocalTheme("space");
          localStorage.setItem("theme", "space");
          play({ id: "space" });
        }}
        className={theme === "space" ? "active" : ""}
      >
        <IoMdPlanet />
      </button>
    </ThemeList>
  );
};

export default withTheme(ThemeSelector);
