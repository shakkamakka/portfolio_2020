import React, { useState } from "react";
import styled from "styled-components";
import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";
import { AniFadeIn } from "./animated/Animations";

const Container = styled.div`
  display: flex;
  position: relative;
  width: 100%;
  align-self: stretch;
  align-items: stretch;
  justify-content: center;
`;
const Arrow = styled.button`
  opacity:${(props) => (props.disabled ? "0" : "0.5")};
  background: none;
  border: none;
  color: #333;
  padding: 12px 16px;
  font-size: 16px;
  cursor: pointer;
  content: ${(props) => (props.direction === "left" ? "left" : "right")};
  transition: opacity 0.5s ease-in;
  &:hover {
    opacity: 1;
  }
`;
const ImageContainer = styled.div`
  width: 80%;
  position: relative;
  animation: ${AniFadeIn} 0.7s linear;

  & img {
    width: 100%;
  }
`;

const Slider = ({ images }) => {
  const [currentImageIndex, setCurrentImageIndex] = useState(0);
  const currentImage = images[currentImageIndex];

  return (
    <Container>
      <Arrow
        disabled={images.length<=1}
        direction="left"
        onClick={() =>
          setCurrentImageIndex(
            currentImageIndex === 0 ? images.length - 1 : currentImageIndex - 1
          )
        }
      >
        <IoIosArrowBack fontSize={40} />
      </Arrow>
      <ImageContainer currentimage={currentImage} key={currentImage}>
        <img alt={currentImage} src={currentImage} />
      </ImageContainer>
      <Arrow
        disabled={images.length<=1}
        direction="right"
        onClick={() =>
          setCurrentImageIndex(
            currentImageIndex === images.length - 1 ? 0 : currentImageIndex + 1
          )
        }
      >
        <IoIosArrowForward fontSize={40} />
      </Arrow>
    </Container>
  );
};

const StyledSlider = styled(Slider)`
  width: 100%;
  border: 1px solid pink;
  height: 30px;
  position: relative;
  margin: 0 auto;
`;

const Carousel = ({ images }) => {
  return <StyledSlider images={images} />;
};

export default Carousel;
