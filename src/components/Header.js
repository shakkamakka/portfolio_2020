import React from "react";
import styled from "styled-components";
import ThemeSelector from "./ThemeSelector";
import { NavLink } from "react-router-dom";

const Section = styled.div`
  display: flex;
  padding: 10px;
  z-index: 1;
  justify-content: flex-end;
  & > * {
    padding: 0 5px;
    line-height: 25px;
  }
`;
const StyledLink = styled(NavLink)`
  text-transform: uppercase;
  color: ${(props) => props.theme.color};
  transition: all 1s;
  opacity:0.5;
  text-shadow: ${(props) =>
    props.$active
      ? "0px 0px 5px " +
        (props.theme.mode === "light" ? "#fff" : props.theme.color)
      : "none"};
  font-size: 0.8em;
  text-decoration: none;
  &:hover, &.active {
    opacity: 1;
    text-shadow: 0px 0px 5px
      ${(props) => (props.theme.mode === "light" ? "#fff" : props.theme.color)};
  }
`;

const Header = ({ setLocalTheme }) => {
  return (
    <Section>
      <ThemeSelector setLocalTheme={setLocalTheme} />
      <StyledLink to="/" >
        Contact
      </StyledLink>
      <StyledLink to="/portfolio">
        Portfolio
      </StyledLink>
    </Section>
  );
};
export default Header;
