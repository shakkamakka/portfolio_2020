import A from './styled/A';
import { AniFadeIn, AniFadeOut, AniBackground } from './animated/Animations';
import Body from './styled/Body';
import ButtonPortfolioType from './styled/ButtonPortfolioType';
import ButtonSocial from './styled/ButtonSocial';
import Container from './styled/Container';
import Footer from './Footer';
import H1 from './styled/H1';
import H2 from './styled/H2';
import Header from './Header';
import Span from './styled/Span';
import Love from './Love';
import ImageProfile from './styled/ImageProfile';
import Social from './Social';
import Stars from './Stars';
import ThemeSelector from './ThemeSelector';
import MoonPhases from './MoonPhases';
import Modal from './Modal';
import Carousel from './Carousel';
import { Row, Column } from './styled/Row';
import Spacer from './styled/Spacer';
import ModalPortfolio from './ModalPortfolio';

export {
	AniFadeOut,
	AniFadeIn,
	AniBackground,
	A,
	Body,
	ButtonPortfolioType,
	Container,
	Footer,
	H1,
	H2,
	Header,
	Love,
	ImageProfile,
	Social,
	Span,
	Stars,
	ThemeSelector,
	MoonPhases,
	Modal,
	Carousel,
	Row,
	Column,
	Spacer,
	ButtonSocial,
	ModalPortfolio,
};
