import React from 'react';
import { A, Modal, Carousel, Row, Column, H2, Spacer } from './';
import parse from "html-react-parser";

const ModalPortfolio = ({ showModal, setShowModal, activeModalData }) => {
	return (
		<Modal showModal={showModal} setShowModal={setShowModal}>
			<Carousel images={activeModalData?.images} />
			<Row>
				<Column width='28'>
					<H2>Info</H2>
					<Spacer />

					<b>Client: </b>
					{activeModalData.client}
					<Spacer />

					<b>Skills: </b>
					{activeModalData.skills}
					<Spacer />

					<Spacer />
				</Column>
				<Column width='70'>
					<H2>Description</H2>
					<Spacer />
					{parse(activeModalData.description)}
					<Spacer />
					<A target='_blank' href={activeModalData.website}>
						{activeModalData.website}
					</A>
					<Spacer />
				</Column>
			</Row>
		</Modal>
	);
};

export default ModalPortfolio;
