import styled from "styled-components";

const ImageProfile = styled.div`
  position: relative;
  display: block;
  color: #e01268;
  width: 150px;
  height: 150px;
  margin: 0 auto;
  background: url("images/m1.png") no-repeat;
  background-size: contain;
  border-radius: 50%;
  padding: 20px;
  border: 1px solid white;
`;

export default ImageProfile;
