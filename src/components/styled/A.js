import styled from "styled-components";

const A = styled.a`
  color: ${(props) => props.theme.color4};
  text-decoration: none;
  display: flex;
  justify-content: space-between;
  align-content: center;
  padding-left: 5px;
  svg {
    line-height: 20px;
    margin-right: 5px;
  }
  &:hover {
    text-decoration: underline;
    color: ${(props) => props.theme.color3};
  }
  &:visited {
    color: ${(props) => props.theme.color4};
  }
`;

export default A;
