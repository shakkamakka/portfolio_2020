import React from "react";
import styled from "styled-components";

const AnchorCircle = styled.a`
  border-radius: 50%;
  color: ${(props) =>
    props.theme.mode === "light" ? `#fff` : props.theme.color};
  display: inline-block;
  position: relative;
  width: 50px;
  height: 50px;
  padding: 5px;
  text-align: center;
  border: 2px solid
    ${(props) =>
      /dark|space/.test(props.theme.mode)
        ? props.theme.socialColors[props.index]
        : props.theme.socialColors};
  &:hover {
    text-shadow: 0px 0px 5px ${(props) => props.theme.color};
  }
  svg {
    font-size: 30px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    color: ${(props) =>
      /dark|space/.test(props.theme.mode)
        ? props.theme.socialColors[props.index]
        : props.theme.socialColors};
  }
`;

const ButtonSocial = ({ item, index }) => {
  return (
    <AnchorCircle href={item.url} index={index} key={index} target="_blank">
      {item.icon()}
    </AnchorCircle>
  );
};

export default ButtonSocial;
