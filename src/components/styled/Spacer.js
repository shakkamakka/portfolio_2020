import styled from "styled-components";

const Spacer = styled.div`
  display: block;
  height: ${(props) => (props.height ? props.height + "px" : "20px")};
`;

export default Spacer;
