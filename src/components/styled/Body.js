import styled from "styled-components";
import { AniBackground } from "../";

const Body = styled.div`
  min-height: 100%;
  text-align: center;
  position: relative;
  background: ${(props) => props.theme.background};
  background-size: 1000% 1000%;
  ${(props) => (props.theme.mode === "space" ? AniBackground : "")};
  font-family: "Open Sans", sans-serif;
  font-family: "Quattrocento Sans", sans-serif;
  display: flex;
  flex-direction: column;
`;
export default Body;
