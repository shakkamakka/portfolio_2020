import {
	FaLinkedinIn,
	FaRegPaperPlane,
	FaCodepen,
	FaSpotify,
	FaChessKnight,
	FaGoodreads,
} from 'react-icons/fa';
import { IoLogoGitlab } from 'react-icons/io5';
import { BsInstagram } from 'react-icons/bs';
import { AiOutlineCodeSandbox } from 'react-icons/ai';
import Tooltip from './styled/Tooltip';
import React from 'react';
import styled from 'styled-components';
import ButtonSocial from './styled/ButtonSocial';

export const socialList = [
	{
		icon: () => <IoLogoGitlab />,
		name: 'Gitlab',
		url: 'https://gitlab.com/shakkamakka',
	},
	{
		icon: () => <FaLinkedinIn />,
		name: 'LinkedIn',
		url: 'https://www.linkedin.com/in/%F0%9F%92%BB-tjankui-lamon-549476161/',
	},
	{
		icon: () => <FaRegPaperPlane />,
		name: 'Mail me',
		url: 'mailto:tjankui@gmail.com',
	},
	{
		icon: () => <BsInstagram />,
		name: 'Instagram',
		url: 'https://www.instagram.com/saso.ri.za/',
	},
	{
		icon: () => <FaCodepen />,
		name: 'Codepen',
		url: 'https://codepen.io/shakkamakka',
	},
	{
		icon: () => <AiOutlineCodeSandbox />,
		name: 'Codesandbox',
		url: 'https://codesandbox.io/u/shakkamakka',
	},
	{
		icon: () => <FaGoodreads />,
		name: 'Goodreads',
		url: 'https://www.goodreads.com/user/show/153016120-shakkamakka',
	},
	{
		icon: () => <FaSpotify />,
		name: 'Spotify',
		url: 'https://open.spotify.com/user/xsasorisa',
	},

	{
		icon: () => <FaChessKnight />,
		name: "Let's play :)",
		url: 'https://www.chess.com/nl-BE/member/xsasorisa',
	},
];

const Section = styled.div`
	display: flex;
	margin: 20px auto;
	display: flex;
	flex-wrap: wrap;
	align-content: space-between;
	height: 230px;
	width: 250px;
	& > div {
		width: 33%;
	}
`;

const Social = () => {
	return (
		<Section>
			{socialList.map((item, index) => (
				<div key={index}>
					<Tooltip
						text={item.name}
						tcolor={(props) => props.theme.socialColors[index] || "black" }
					>
						<ButtonSocial item={item} index={index} />
					</Tooltip>
				</div>
			))}
		</Section>
	);
};

export default Social;
