import React from "react";
import styled from "styled-components";
import {
  WiMoonWaningCrescent6,
  WiMoonWaxingCrescent1,
  WiMoonFirstQuarter,
  WiMoonWaxingGibbous6,
  WiMoonFull,
  WiMoonWaningGibbous2,
  WiMoonThirdQuarter,
  WiMoonWaningCrescent2,
  WiMoonWaxing6,
} from "react-icons/wi";

const Phases = styled.div`
  display: flex;
  justify-content: center;
  color: url(${(props) => props.theme.color});
  opacity: 0.1;
  svg {
    font-size: 30px;
  }
`;

const MoonPhases = () => {
  return (
    <Phases>
      <WiMoonWaxingCrescent1 />
      <WiMoonWaxing6 />
      <WiMoonFirstQuarter />
      <WiMoonWaxingGibbous6 />
      <WiMoonFull />
      <WiMoonWaningGibbous2 />
      <WiMoonThirdQuarter />
      <WiMoonWaningCrescent2 />
      <WiMoonWaningCrescent6 />
    </Phases>
  );
};

export default MoonPhases;
