import React from "react";
import styled from "styled-components";
import { AniFadeIn, H1, H2, ImageProfile, Social, Span } from "../components";

const Centered = styled.div`
  & > * {
    animation: ${AniFadeIn} 1s forwards;
  }
`;

const Contact = () => {
  return (
    <>
      <Centered>
        <ImageProfile />
        <H1>Tjankui Lamon</H1>
        <H2>Frontend developer / UI designer</H2>
        <Span>You can stalk me online at following places:</Span>
        <Social />
      </Centered>
    </>
  );
};

export default Contact;
