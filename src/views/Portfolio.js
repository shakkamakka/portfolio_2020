import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { Flipper, Flipped } from "react-flip-toolkit";
import {
  ButtonPortfolioType,
  H2,
  H1,
  Span,
  Spacer,
  ModalPortfolio,
} from "../components";
import { portfolioItems } from "../portfolio/items";
import { shuffle } from "../utils/shuffle";

// Filter unique types
const uniqueTypeArrays = portfolioItems.map((section) =>
  Object.values(section.type)
);
const uniqueTypes = Array.from(new Set([].concat.apply([], uniqueTypeArrays)));

// Styling
const FlipperContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  width: 100%;
  margin: 10px auto;

  & div {
    width: 30%;
    height: 250px;
    margin-bottom: 20px;
  }

  @media (max-width: 1300px) {
    & div {
      width: 50%;
      min-height: 250px;
    }
  }

  @media (max-width: 768px) {
    display: block;

    & div {
      width: 100%;
      min-height: 250px;
    }
  }
`;
const FlippedItem = styled.div`
  background: url(${(props) => props.thumb}) no-repeat center center;
  background-size: contain;
  cursor: pointer;
  position: relative;
  &:hover::before {
    position: absolute;
    content: "";
    display: block;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.5);
    cursor: pointer;
  }
`;
const Container = styled.div`
  margin: 10px auto;
  padding: 10px 20px;
  background: ${(props) => (props.theme.mode === "light" ? "#fff" : "none")};
  width: 80%;
  box-shadow: ${(props) =>
    props.theme.mode === "light"
      ? "0px 1px 2px rgba(166, 175, 195, 0.25)"
      : "none"};
  border-radius: 5px;
`;

const Portfolio = () => {
  const [items, setItems] = useState(portfolioItems);
  const [data, setData] = useState(portfolioItems.map((section) => section.id));
  const [showModal, setShowModal] = useState(false);
  const [activeModalData, setActiveModalData] = useState(
    portfolioItems.filter(({ id }) => id === 1)[0]
  );
  const [portfolioType, setPortfolioType] = useState("All");

  const handleClick = (type) => {
    setPortfolioType(type);
    if (type === "All") return setItems(portfolioItems);
    const filtered = portfolioItems.filter(
      (section) => section["type"].indexOf(type) !== -1
    );
    setItems(shuffle(filtered));
  };

  useEffect(() => {
    setData(items.map((section) => section.id));
  }, [items]);

  return (
    <div>
      {/* Intro */}
      <H1>Portfolio</H1>
      <H2 largepadding>Some work I made as a freelancer.</H2>
      <Spacer />
      <Span>
        There is more to come and there are also the projects I have worked on
        as a consultant.
      </Span>
      <Span>
        Should you be interested in the latter, feel free to contact me :)
      </Span>
      <Spacer />
      {/* Modal */}
      <ModalPortfolio
        showModal={showModal}
        setShowModal={setShowModal}
        activeModalData={activeModalData}
      />

      {/* category */}
      <Container>
        <ButtonPortfolioType
          text="All"
          handleClick={handleClick}
          index={uniqueTypes.length}
          active={portfolioType === "All"}
        />
        {uniqueTypes.map((type, index) => (
          <ButtonPortfolioType
            handleClick={handleClick}
            key={index}
            index={index}
            text={type}
            active={portfolioType === type}
          />
        ))}
        <br />

        {/* gallery */}
        <Flipper flipKey={data.join("")}>
          <FlipperContainer>
            {data.map((d) => (
              <Flipped key={d} flipId={d}>
                <FlippedItem
                  thumb={portfolioItems.filter((s) => s.id === d)[0].thumb}
                  onClick={() => {
                    setActiveModalData(
                      portfolioItems.filter(({ id }) => id === d)[0]
                    );
                    setShowModal(true);
                  }}
                >
                  <div></div>
                </FlippedItem>
              </Flipped>
            ))}
          </FlipperContainer>
        </Flipper>

      </Container>
    </div>
  );
};

export default Portfolio;
