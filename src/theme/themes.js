// media queries
const s = "500px";
const m = "750px";
const l = "1000px";
// space
const blue = "#3bcdd7";
const pink = "#e01268";
const lightpink = "#e01268";
const green = "#18c1a6";
const red = "#e4554e";
const yellow = "#f5bc33";
const purple = "#ac20e7";
// dark
const d_black = "#0E1419";
const d_cyan = "#8BE9FD";
const d_yellow = "#ffb86c";
const d_magenta = "#ff79c6";
const d_green = "#50fa7b";
const d_purple = "#bd93f9";
//------------------------------
// themes
export const spaceTheme = {
  gradient: "rgba(0, 0, 0, 0.4)",
  title_color: "#fff",
  subtitle: green,
  color: "#fff",
  color0: "#000",
  color1: blue,
  color2: pink,
  color3: lightpink,
  color4: green,
  color5: red,
  font1: `"BIZ", sans-serif`,
  font2: `"Lato", sans-serif`,
  medium: m,
  mode: "space",
  maintitle: "#fff",
  title_spacing: "-1px",
  title_transform: "uppercase",
  subtitle_spacing: "2px",
  subtitle_border: "1px",
  span_color: "white",
  background:
    "linear-gradient(242deg, #364a79, #352c53, #543728, #18494b, #141321)",
  navButton: blue,
  navButtonHover: "#fff",
  navCloseButton: pink,
  navColor: blue,
  navColorActive: "#fff",
  navColorDrawer: blue,
  navColorDrawerActive: "#fff",
  navBG: "rgba(0,0,0, 0.9)",
  footerheight: "50px",
  themeActive: pink,
  socialColors: [pink, blue, red, green, yellow, purple, red, blue, pink],
  aboutbg: "rgba(0, 0, 0, 0.4)",
};
export const darkTheme = {
  color: "#FFF",
  color0: d_black,
  color1: d_green,
  color2: d_magenta,
  color3: d_yellow,
  color4: d_purple,
  color5: d_cyan,
  font1: `"Roboto", sans-serif`,
  font2: `"Lato", sans-serif`,
  medium: m,
  large: l,
  small: s,
  mode: "dark",
  link: d_purple,
  title_color: d_yellow,
  title_spacing: "3px",
  title_transform: "none",
  subtitle: d_purple,
  subtitle_spacing: "1px",
  subtitle_border: "1px",
  span_color: "white",
  background: "#0E1419",
  navButton: d_purple,
  navButtonHover: d_magenta,
  navCloseButton: d_purple,
  navCloseButtonHover: d_magenta,
  navColor: d_green,
  navColorActive: d_magenta,
  navColorDrawer: d_green,
  navColorDrawerActive: d_magenta,
  navBG: "rgba(0,0,0, 0.9)",
  footerheight: "50px",
  themeActive: d_magenta,
  socialColors: [d_purple, d_magenta, d_green, d_green, d_yellow,d_cyan ,d_yellow, d_magenta,  d_purple]
};
export const lightTheme = {
  color: "#333",
  color0: "#333",
  color1: d_cyan,
  color2: d_yellow,
  color3: d_magenta,
  color4: "#888",
  color5: d_purple,
  font1: `"Roboto", sans-serif`,
  font2: `"Lato", sans-serif`,
  medium: m,
  large: l,
  small: s,
  mode: "light",
  title_color: "#888",
  title_spacing: "1px",
  title_transform: "none",
  subtitle_spacing: "0",
  subtitle_border: "3px",
  span_color: "#333",
  maintitle: d_yellow,
  background: "#f9f9f8",
  navButton: "#333",
  navButtonHover: "#000",
  navCloseButton: "#333",
  navCloseButtonHover: "#000",
  navColor: "#333",
  navColorActive: "#000",
  navColorDrawer: "#333",
  navColorDrawerActive: "#000",
  navBG: "#eee",
  footerheight: "50px",
  themeActive: "#111",
  socialColors: "#333",
  aboutbg: "rgba(0, 0, 0, 0.4)",
};
